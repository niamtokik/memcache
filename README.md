# memcache

memcache protocol implementation in Erlang.

## Usage

```sh
rebar3 compile
```

## Test

```sh
rebar3 ct
rebar3 eunit
```

# Resources and References

 * https://github.com/memcached/memcached/blob/master/doc/protocol.txt

# About

Made with <3 by Mathieu Kerjouan with [Erlang](erlang.org/) and
[rebar3](https://www.rebar3.org).
